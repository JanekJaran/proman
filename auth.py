from flask import Blueprint, render_template, request, session, redirect, \
    url_for, flash
from datbase import check_user_exist_by_username, add_user_by_username, \
    get_user_by_username
import bcrypt
import datetime

auth = Blueprint('auth', __name__, )


@auth.route("/register", methods=["GET", "POST"])
def register():
    if request.method == "GET":
        return render_template("register.html")
    if not ["email", "username", "password"] == list(request.form):
        return render_template("register.html")
    # TODO Dodac walidacje - Czy mail zajety?
    if check_user_exist_by_username(request.form['username']):
        # username zajety
        print("Zajęty")
        flash("Nazwa jest zajęta.")
        return render_template("register.html")
    if len(request.form['password']) < 8:
        # Haslo za krotkie
        return render_template("register.html")
    passhash = str(bcrypt.hashpw(request.form['password'].encode(),
                                 bcrypt.gensalt())).replace("\'", "\'\'")
    user = {"username": request.form['username'],
            "email": request.form['email'],
            "passhash": passhash, "register_date": datetime.datetime.now()}
    add_user_by_username(user)
    user = get_user_by_username(request.form['username'])
    session["login"] = user["id"]
    return redirect(url_for("index"))


@auth.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "GET":
        return render_template("login.html")
    if not ["username", "password"] == list(request.form):
        flash("Należy podać nazwę i hasło.")
        return render_template("login.html")
    if not check_user_exist_by_username(request.form['username']):
        return render_template("login.html")
    user = get_user_by_username(request.form["username"])
    session["login"] = user["id"]
    return redirect(url_for("index"))


@auth.route("/logout")
def logout():
    session["login"] = ''
    redirect(url_for("auth.login"))
