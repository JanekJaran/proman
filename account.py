from flask import Blueprint, render_template, request, session, redirect, \
    url_for
from datbase import check_user_exist_by_username, add_user_by_username, \
    get_user_by_username, get_user_by_id

account = Blueprint('account', __name__, )


@account.route("/home")
def home():
    if not session.get("login"):
        return redirect(url_for("auth.login"))
    user = get_user_by_id(session["login"])
    return render_template("home.html", user=user)
