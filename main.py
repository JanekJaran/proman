from flask import Flask, render_template, url_for

from api import api
from auth import auth
from account import account
from util import json_response

app = Flask(__name__)
app.register_blueprint(auth)
app.register_blueprint(account)
app.register_blueprint(api)
app.secret_key = "gsedsdrgrgrgrgwewegrewgfwefgwefgweowsdvojnkwsdfv"


@app.route("/")
def index():
    """
    This is a one-pager which shows all the boards and cards
    """
    return render_template('index.html')


def main():
    app.run(debug=True)

    # Serving the favicon
    with app.app_context():
        app.add_url_rule('/favicon.ico', redirect_to=url_for('static',
                                                             filename='favicon/favicon.ico'))


if __name__ == '__main__':
    main()
