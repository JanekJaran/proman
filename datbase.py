import psycopg2

conn = psycopg2.connect(
    host="localhost",
    database="proman",
    user="postgres",
    password="kluski1234")
c = conn.cursor()


def user_dict(user):
    return {"id": user[0], "username": user[1], "email": user[2],
            "passhash": user[3], "register_date": user[4]}


def board_dict(board):
    return {"id": board[0], "title": board[1], "owner_id": board[2]}


def card_dict(card):
    return {"id": card[0], "board_id": card[1], "title": card[2],
            "status_id": card[3], "order": card[4]}


def get_user_by_id(id):
    c.execute('''SELECT * FROM users WHERE id='{}' '''.format(id))
    user = c.fetchone()
    return user_dict(user)


def get_user_by_username(username):
    c.execute('''SELECT * FROM users WHERE username='{}' '''.format(username))
    user = c.fetchone()
    return user_dict(user)


def add_user_by_username(user):
    c.execute(
        '''INSERT INTO users (username, email, passhash, register_date) VALUES ('{}', '{}', '{}', '{}')'''.format(
            user["username"], user["email"], user["passhash"],
            user["register_date"]))
    conn.commit()


def check_user_exist_by_username(username):
    c.execute(
        '''SELECT COUNT(*) FROM users WHERE username='{}' '''.format(username))
    count = c.fetchone()[0]
    print(count, "/////")
    if count > 0:
        print(True)
        return True
    print(False)
    return False


def get_board_by_id(id):
    c.execute('''SELECT * FROM boards WHERE id='{}' '''.format(id))
    board = c.fetchone()
    return board_dict(board)


def get_public_boards():
    c.execute('''SELECT * FROM boards WHERE access=true''')
    boards = c.fetchall()
    result = []
    for board in boards:
        result.append(board_dict(board))
    return result


def get_cards_by_borad(board_id):
    c.execute('''SELECT * FROM cards WHERE board_id='{}' '''.format(board_id))
    cards = c.fetchall()
    result = []
    for card in cards:
        result.append(board_dict(card))
    return result
