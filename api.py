from flask import Blueprint, render_template, request, session, redirect, \
    url_for, jsonify
from datbase import check_user_exist_by_username, add_user_by_username, \
    get_user_by_username, get_user_by_id, get_public_boards, get_cards_by_borad

api = Blueprint('api', __name__)


@api.route("/api/boards")
def list_boards():
    return jsonify(get_public_boards())


@api.route("/api/board/<id>")
def cards_by_board(id):
    return jsonify(get_cards_by_borad(id))
